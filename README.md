# fitting-export


This is an extension to the [Fittings](https://gitlab.com/colcrunch/fittings) for [Alliance Auth](https://gitlab.com/allianceauth/allianceauth)

## Installation

### Step 1: Install the App

```shell
pip install git+https://gitlab.com/TTDulf/fittings-export.git
```
### Step 2: Install the App

Add `fittings_export` to your `INSTALLED_APPS`.

### Step 3: Run migrations

```shell
python manage.py migrate
```

### Step 4: Set up permissions

The `fittings_export | fittings export | Can export fittings` permissions allows for exporting of fittings.