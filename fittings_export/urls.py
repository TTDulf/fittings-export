from django.urls import re_path

from . import views

app_name = 'fittings_export'

urlpatterns = [
    re_path(r'^$', views.dashboard, name='dashboard'),
    re_path(r'^doctrine/(?P<doctrine_id>[0-9]+)/$', views.export_doctrine, name='export_doctrine'),
]