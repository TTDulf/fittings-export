from django.apps import AppConfig


class FittingsExportConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'fittings_export'
