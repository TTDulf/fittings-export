from allianceauth import hooks
from allianceauth.services.hooks import UrlHook, MenuItemHook

from . import urls

class FittingMenu(MenuItemHook):
    def __init__(self):
        MenuItemHook.__init__(self, 'Export Fittings',
                              'far fa-list-alt fa-fw', 'fittings_export:dashboard',
                              navactive=['fittings_export:'])

    def render(self, request):
        if request.user.has_perm('fittings_export.fittings_export'):
            return MenuItemHook.render(self, request)
        return ''


@hooks.register('menu_item_hook')
def register_menu():
    return FittingMenu()

@hooks.register('url_hook')
def register_url():
    return UrlHook(urls, 'fittings_export', '^export/')