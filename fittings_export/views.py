from allianceauth.services.hooks import get_extension_logger
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.utils.translation import gettext as gt
from django.db.models import Prefetch
from django.shortcuts import render, redirect

from fittings.models import Doctrine, Fitting
from fittings.views import _check_doc_access, _get_docs_qs

from django.http import HttpResponse

from xml.dom import minidom

# Create your views here.
@permission_required('fittings_export.fittings_export')
@login_required()
def dashboard(request):
    groups = request.user.groups.all()
    docs = _get_docs_qs(request, groups)
    
    ctx = {'docs': docs}
    
    return render(request, 'fittings_export/dashboard_export.html', context=ctx)

@permission_required('fittings_export.fittings_export')
@login_required()
def export_doctrine(request, doctrine_id):
    try:
        doctrine = Doctrine.objects.prefetch_related('category')\
            .prefetch_related(Prefetch('fittings', Fitting.objects.select_related('ship_type')))\
            .prefetch_related('fittings__category')\
            .prefetch_related('fittings__doctrines')\
            .prefetch_related('fittings__doctrines__category').get(pk=doctrine_id)
    except Doctrine.DoesNotExist:
        messages.warning(request, gt('Doctrine not found!'))

        return redirect('fittings_export:dashboard')

    # Check if the user should be able to access the doctrine.
    access = _check_doc_access(request, doctrine_id)

    if not access:
        messages.warning(request, gt('You do not have access to that doctrine.'))

        return redirect('fittings_export:dashboard')

    assert True
    
    doc = minidom.Document()
    fittings = doc.createElement('fittings')
    doc.appendChild(fittings)
    
    for fit in doctrine.fittings.all():
        fitting = doc.createElement('fitting')
        fitting.setAttribute('name', fit.name)
        fittings.appendChild(fitting)
        description = doc.createElement('description')
        description.setAttribute('value', fit.description)
        fitting.appendChild(description)
        shipType = doc.createElement('shipType')
        #fittings v1.1.0 uses type_name and later versions use name
        shipTypeDict = fit.ship_type.__dict__
        shipType.setAttribute('value', shipTypeDict.get('name') or shipTypeDict.get('type_name'))
        fitting.appendChild(shipType)
        for item in fit.items.all():
            hardware = doc.createElement('hardware')
            #fittings v1.1.0 uses type_name and later versions use name
            itemTypeDict = item.type_fk.__dict__
            hardware.setAttribute('type', itemTypeDict.get('name') or itemTypeDict.get('type_name'))
            if item.flag == 'Cargo':
                hardware.setAttribute("qty", "%d" % item.quantity)
                hardware.setAttribute("slot", "cargo")
            if item.flag == 'DroneBay':
                hardware.setAttribute("qty", "%d" % item.quantity)
                hardware.setAttribute("slot", "drone bay")
            if item.flag == 'FighterBay':
                hardware.setAttribute("qty", "%d" % item.quantity)
                hardware.setAttribute("slot", "fighter bay")
            if item.flag.startswith("HiSlot"):
                hardware.setAttribute("slot", "hi slot %s" % item.flag[-1])
            if item.flag.startswith("MedSlot"):
                hardware.setAttribute("slot", "med slot %s" % item.flag[-1])
            if item.flag.startswith("LoSlot"):
                hardware.setAttribute("slot", "low slot %s" % item.flag[-1])
            if item.flag.startswith("RigSlot"):
                hardware.setAttribute("slot", "rig slot %s" % item.flag[-1])
            if item.flag.startswith("ServiceSlot"):
                hardware.setAttribute("slot", "service slot %s" % item.flag[-1])
            if item.flag.startswith("SubSystemSlot"):
                hardware.setAttribute("slot", "subsystem slot %s" % item.flag[-1])
            fitting.appendChild(hardware)
    
    text = doc.toxml(encoding='utf-8')
    
    response = HttpResponse(text, content_type="application/xml")
    response['Content-Disposition'] = 'attachment; filename=%s.xml' % (doctrine.name + doctrine.last_updated.strftime('%Y%m'))
    return response