from django.db import models

# Create your models here.

class FittingsExport(models.Model):
    """
    Meta model for app permissions
    """
    
    class Meta:
        """
        Meta definitions
        """
        
        managed = False
        default_permissions = ()
        permissions = (("fittings_export", "Can export fittings"),)